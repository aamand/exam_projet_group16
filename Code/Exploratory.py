import pandas as pd
import matplotlib.pyplot as plt
import math

path = "./Data/"

coffee_df = pd.read_csv(path + "Coffeebar_2013-2017.csv", sep=';')
# db reform
coffee_df['HOUR'] = coffee_df['TIME'].str[-8:]  # we clarify the df by separating date and hour
coffee_df['DATE'] = coffee_df['TIME'].str[:10]
coffee_df['YEAR'] = coffee_df['DATE'].str[:4]  # we add a row 'YEAR' to simplify our futur code


#QUESTION 1

print("List of drink sold : ",coffee_df['DRINKS'].unique())
print("List of food sold",coffee_df['FOOD'].unique())
nbr_unique_custo = coffee_df['CUSTOMER'].unique() #nbr of unique customers
print("Number of unique customer", nbr_unique_custo.shape[0])


#QUESTION 2

nbr_unique_hour = coffee_df['HOUR'].unique()
nbr_year = coffee_df['YEAR'].unique()

#plot of amount of food sold over the five year
food_by_year = coffee_df.groupby(pd.DatetimeIndex(coffee_df.TIME).year).agg({'FOOD': 'count'})
ax = food_by_year.plot(kind='bar', title="Nbr of sold food by year", figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Year", fontsize=12)
ax.set_ylabel("FOOD", fontsize=12)
plt.ylim([32600,33100])
plt.show()

#plot of amount of food sold over the five years
food_by_year = coffee_df['FOOD'].value_counts()
ax = food_by_year.plot(kind='bar', title="Food by type", figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Type", fontsize=12)
ax.set_ylabel("FOOD", fontsize=12)
plt.show()

#plot of amount of drink sold over the five years
drinkByYear = coffee_df['DRINKS'].value_counts()
ax = drinkByYear.plot(kind='bar', title="Drinks by type", figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Year", fontsize=12)
ax.set_ylabel("DRINKS", fontsize=12)
plt.show()



#plot of average income by hour : see in the part 4 Additional

#plot of income by hour : see in the part 4 Additional

#plot of income by year : see in the part 4 Additional

#plot of average daily income by year : see in the part 4 Additional

#plot of oneTime and returning customer : see in the part 4 Additional

#plot of number of returning customer during the day : see in the part 4 Additional

#plot of number of one-time customer during the day : see in the part 4 Additional

#plot of income of one-time customer by year : see in the part 4 Additional

#plot of income of returning customer by year : see in the part 4 Additional

#QUESTION 3


coffee_df['FOOD'] = coffee_df['FOOD'].fillna('nothing')  #to replace missing value by the str : "nothing"
def average_customer_purchase(time):
    total = coffee_df[coffee_df['HOUR']== time]  #select the lines of the asking hour
    nbr_total = total.shape[0]                 #count the nbr of client of the asking hour
    all_drinks = total['DRINKS'].unique()      #create a list with all the different possible drink
    all_food = total['FOOD'].unique()          #create a list with all the different possible food

    dico_drink = {}  # dictio with key = name of the drink sold and value = a tuple [nbr of drink sold and pourcentage]
    for drink in all_drinks:
        nbr_precise_drink = (total[total['DRINKS'] == drink]).shape[0]  #nbr of drink (example coffee)sold at the asking hour
        dico_drink[drink] = [nbr_precise_drink, round((float(nbr_precise_drink)/ nbr_total)*100, 2) ]
                                                # we use round to have only two comma
    dico_food = {}    # same as drink
    for food in all_food:
        nbr_precise_food = (total[total['FOOD'] == food]).shape[0]
        dico_food[food] = [nbr_precise_food, round((float(nbr_precise_food)/nbr_total)*100, 2)]

    #I put all the data founded above in two dataframe
    df_q3_drink = pd.DataFrame({ 'DRINKS' : [e for e in all_drinks],
                           'PROBABILITY' : [dico_drink[e][1] for e in all_drinks]
    })
    df_q3_food = pd.DataFrame({ 'FOOD' : [e for e in all_food],
                                'PROBABILITY' : [dico_food[e][1] for e in all_food]
    })

#I choose to give the probability in a dataframe because I think it is more visual than a sentence .
    print("On average the probabilities of a customer to buy something at "+ time +" are on the dataframes below:")
    print(df_q3_drink)
    print()    #to start a new line, unfortunatly I didn't find anything more elegant than that.
    print(df_q3_food)


average_customer_purchase('12:00:00') #to test my function
