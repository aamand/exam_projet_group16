import pandas as pd
import matplotlib.pyplot as plt
from random import randint
from datetime import timedelta, date
from Code import Customers
path = "./Data/"
coffee_df = pd.read_csv(path+"Coffeebar_2013-2017.csv", sep=';')
coffee_df['HOUR'] = coffee_df['TIME'].str[-8:]
coffee_df['DATE'] = coffee_df['TIME'].str[:10]
coffee_df['YEAR'] = coffee_df['DATE'].str[:4]
nbr_unique_hour = coffee_df['HOUR'].unique()
nbr_year = coffee_df['YEAR'].unique()

#function giving the prize of the asking drink
def give_drinkPrize(drink):
    price = 0
    if drink in ["coffee", "water", "milkshake", "tea", "frappucino", "soda"]:  # list of sold drink, founded in part 1
        if drink == "milkshake":
            price = 5
        elif drink == "frappucino":
            price = 4
        elif drink == "water":
            price = 2
        else:
            price = 3
    #else:
     #   return ("The mentionned drink is not available in the coffee bar")
     #  to avoid crashing the program. because of the simulation, we don't use this safety because it is useless in our case
    return price

#same as the function above
def give_foodPrize(food):
    price = 0
    if food in ["sandwich", "cookie", "pie", "muffin", "nothing"]:
        if food == "sandwich":
            price = 5
        elif food == "cookie":
            price = 2
        elif food == "nothing":
            price = 0
        else:
            price = 3
    #else:
        #return ("The mentionned food is not available in the coffee bar")
    return price
def average_customer_purchase(time):
    total = coffee_df[coffee_df['HOUR']== time]
    nbr_total = total.shape[0]
    all_drinks = total['DRINKS'].unique()
    all_food = total['FOOD'].unique()
    dico_food = {}
    dico_drink = {}
    for drink in all_drinks:
        nbr_precise_drink = (total[total['DRINKS'] == drink]).shape[0]
        dico_drink[drink] = round((float(nbr_precise_drink)/ nbr_total)*100, 2)

    for food in all_food:
        nbr_precise_food = (total[total['FOOD'] == food]).shape[0]
        dico_food[food] = round((float(nbr_precise_food)/nbr_total)*100, 2)
    list_purchases = [dico_drink, dico_food]
    return (list_purchases)

def give_purchase(time):

    #finding drink depending on the probability
    dico_drink = average_customer_purchase(time)[0]
    intervalle_drink_proba = {}
    temp_drink = 0
    for drink in dico_drink:
        intervalle_drink_proba[drink] = dico_drink[drink] + temp_drink
        temp_drink += dico_drink[drink]
    proba = randint(1,99)
    for purchase in intervalle_drink_proba:
        if proba <= intervalle_drink_proba[purchase]:
            drink_ordered = purchase
            break

    #finding food depending on the probability
    dico_food = average_customer_purchase(time)[1]
    intervalle_food_proba = {}
    temp_food = 0
    for food in dico_food:
        intervalle_food_proba[food] = dico_food[food] + temp_food
        temp_food += dico_food[food]
    proba = randint(1,99)
    #print('proba :', proba)
    #print('list_food : ', intervalle_food_proba)
    for purchase in intervalle_food_proba:
        if proba <= intervalle_food_proba[purchase]:
            food_ordered = purchase
            break

    return drink_ordered, food_ordered

#PART4 : Question 1 : show some buying histories :
def date_range(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)

def db_simulation(start_date, end_date):
    history = {}
    list_returning_id = []
    for cust in range (1000): #we create the base of database with the returning customer
        if randint(1,100) <= 33: #about a third must be hipster ones
            customer = Customers.Customer(len(history),False,False) #creation of the hipsters
            history = customer.order(history) #updating the general history with the personal history of the created customers
        else:
            customer = Customers.Customer(len(history),False,True) #creation of the returning regular customers
            history = customer.order(history) #updating the general history with the personal history of the created customers
        list_returning_id.append(str(len(history))) #stock all the id of the returning customers (in range from 0 to 999)
    #we will browse all day and all hour during 5 years

    for date in date_range(start_date, end_date):
        date = date.strftime("%Y-%m-%d")
        all_hour = coffee_df['HOUR'].unique()
        dico_simulation = {'date':[],'hour': [], 'id': [], 'drink': [], 'food': []}
        for hour in all_hour: #we browse each hour
            if randint(1,10) <=2: #that means we have a returning customer (20% of the orders)
                success = False
                while success == False:
                    rand = randint(0,999) #getting randomly the id of a returning customer
                    if history[str(rand)]['budget'] >= 10: #check if the budget is enough to buy any food and any drink
                        customer_id = str(rand)
                        customer_type = history[customer_id]["customer_type"]
                        oneTime_flag = False
                        if customer_type[-7:] == "regular":
                            regular_flag = True
                        else :
                            regular_flag = False
                        success = True
                customer_type = "returning"
            else:  #that means we have a one time customer
                if randint(1, 10) <= 1: #creation of a tripadvisor customer (10% of the one time ones)
                    customer = Customers.Customer(len(history), True, False) #creation of the customer
                    history = customer.order(history) #updating the general history with the personal history of the new customer
                else: #creation of one time regular customer
                    customer = Customers.Customer(len(history), True, True)
                    history = customer.order(history) #updating the general history with the personal history of the new customer
                customer_id = str(len(history)) #stoch the current id of the customer
                customer_type =  "one time"
            if customer_type == "returning": #if returning customer we create again the object to use it just after
                customer = Customers.Customer(customer_id, oneTime_flag, regular_flag)
            purchase = give_purchase(hour) #getting the list of the  ordered drink and food
            drink = purchase[0] #getting the ordered drink
            food = purchase[1] #getting the ordered food
            history = customer.buy_sth(drink,food,date,hour,history) #updating the general history after an order
            # updating de DF
            dico_simulation['date'].append(date)
            dico_simulation['hour'].append(hour)
            dico_simulation['id'].append(customer_id)
            dico_simulation['drink'].append(drink)
            dico_simulation['food'].append(food)
            if customer_type == "returning":
                print(date, hour, customer_id,drink, food)
                print(history[customer_id]["customer_type"])
                print(customer.access_history_drinkSpending(history))
                print(customer.access_history_foodSpending(history))
                print(customer.access_history_budget(history))
                print('')

    my_df = pd.DataFrame(dico_simulation)

    return my_df
start_date = date(2018, 1, 1)
end_date = date(2023, 1, 1)
db = db_simulation(start_date, end_date)

#PART 4 : Question 2 :

#creation of "returning" row to sort the returningg (=True) and onetime customer (=False)
valueCountAllCusto = coffee_df['CUSTOMER'].value_counts()
valueCountReturingCusto = valueCountAllCusto[valueCountAllCusto > 1]
returningCustoKey= valueCountReturingCusto.keys()
def returningCust(x):
    if x in returningCustoKey:
        return True
    else:
        return False
coffee_df['RETURNING'] = coffee_df['CUSTOMER'].map(returningCust)

#Number of returning customer
print('Number of returning customer : '+str(len(returningCustoKey)))


# Can you determine a probability of having a onetime or returning customer at a given time?
def ReturningOneTimeProba(time):
    hour_df = coffee_df[coffee_df['HOUR'] == time]
    nbr = (hour_df['RETURNING'] == True).value_counts()
    print("The probability of having a returning customer is : " + str(round((nbr[1]/(nbr[0]+nbr[1]))*100, 2)) +'%.')
    print("The probability of having a one-time customer is : " + str(round((nbr[0]/(nbr[0]+nbr[1]))*100, 2)) +'%.')
    return (round((nbr[1]/(nbr[0]+nbr[1]))*100, 2),round((nbr[0]/(nbr[0]+nbr[1]))*100, 2))


ReturningOneTimeProba('17:40:00')



#Do you see correlations between what returning customers buy and one-timers?
#We can see a correlation between the number of returning customer and the amount of food sold over the day. We have less
#returning customer during midday and the amount of food increase during midday. So we could think the one-time customer


#PART4 : Question 3 : What would happen if the number of returning custmers drops to 50 ?

def db_simulation(start_date, end_date):
    history = {}
    list_returning_id = []
    for cust in range (50): #we create the base of database with the returning customer
        if randint(1,100) <= 33: #about a third must be hipster ones
            customer = Customers.Customer(len(history),False,False) #creation of the hipsters
            history = customer.order(history) #updating the general history with the personal history of the created customers
        else:
            customer = Customers.Customer(len(history),False,True) #creation of the returning regular customers
            history = customer.order(history) #updating the general history with the personal history of the created customers
        list_returning_id.append(str(len(history))) #stock all the id of the returning customers (in range from 0 to 999)
    #we will browse all day and all hour during 5 years

    for date in date_range(start_date, end_date):
        date = date.strftime("%Y-%m-%d")
        all_hour = coffee_df['HOUR'].unique()
        dico_simulation = {'date':[],'hour': [], 'id': [], 'drink': [], 'food': []}
        for hour in all_hour: #we browse each hour
            if randint(1,10) <=2: #that means we have a returning customer (20% of the orders)
                success = False
                while success == False:
                    rand = randint(0,49) #getting randomly the id of a returning customer
                    if history[str(rand)]['budget'] >= 10: #check if the budget is enough to buy any food and any drink
                        customer_id = str(rand)
                        customer_type = history[customer_id]["customer_type"]
                        oneTime_flag = False
                        if customer_type[-7:] == "regular":
                            regular_flag = True
                        else :
                            regular_flag = False
                        success = True
                customer_type = "returning"
            else:  #that means we have a one time customer
                if randint(1, 10) <= 1: #creation of a tripadvisor customer (10% of the one time ones)
                    customer = Customers.Customer(len(history), True, False) #creation of the customer
                    history = customer.order(history) #updating the general history with the personal history of the new customer
                else: #creation of one time regular customer
                    customer = Customers.Customer(len(history), True, True)
                    history = customer.order(history) #updating the general history with the personal history of the new customer
                customer_id = str(len(history)) #stoch the current id of the customer
                customer_type =  "one time"
            if customer_type == "returning": #if returning customer we create again the object to use it just after
                customer = Customers.Customer(customer_id, oneTime_flag, regular_flag)
            purchase = give_purchase(hour) #getting the list of the  ordered drink and food
            drink = purchase[0] #getting the ordered drink
            food = purchase[1] #getting the ordered food
            history = customer.buy_sth(drink,food,date,hour,history) #updating the general history after an order
            # updating de DF
            dico_simulation['date'].append(date)
            dico_simulation['hour'].append(hour)
            dico_simulation['id'].append(customer_id)
            dico_simulation['drink'].append(drink)
            dico_simulation['food'].append(food)

    my_df = pd.DataFrame(dico_simulation)

    return my_df

db = db_simulation(start_date, end_date)


#PART4 (by Alexandre): Question 4 : The prices change from the beginning of 2015 and go up by 20%
#We made our simulation between 2018 and 2022 then we consider that the change will be in the beginning of 2020.

def give_foodPrizeAfter2015(food):  #function which returns the price of the ordered food
    if food in ["sandwich", "cookie", "pie", "muffin", "nothing"]:  # list of all the food we can find in the coffee bar
        if food == "sandwich":
            price = 5
        elif food == "cookie":
            price = 2
        elif food == "nothing":
            price = 0
        else:
            price = 3
    else: #error message if the ordered food is unknown or just not available in the coffee bar
        return ("The mentionned food is not available in the coffee bar")  # Pour éviter une erreur dans le programme
    price = price + 0.2 * (price)
    return price

def give_drinkPrizeAfter2015(drink): #function wich returns the price of the ordered drink
    if drink in ["coffee", "water", "milkshake", "tea", "frappucino", "soda"]:  #list of all the drinks we can find in the coffee bar
        if drink == "milkshake":
            price = 5
        elif drink == "frappucino":
            price = 4
        elif drink == "water":
            price = 2
        else:
            price = 3
    else: #error message if the ordered food is unknown or just not available in the coffee bar
        return ("The mentionned drink is not available in the coffee bar")  # Pour éviter une erreur dans le programme
    price = price + 0.2*(price)
    return price

#PART4 : Question 5 : The budget of hipsters drops to 40

def order (self, history): #function to update the general history with a new customer
    self.history = history #general history of the coffee bar with all the customers
    if self.oneTime_flag == False : #creation of a returning customer
        #print('Welcome back customer'+' '+str(self.id))
        if self.regular_flag == True:
            customer_type = "returning regular"
            budget = 250
        else:
            customer_type = "returning hipster"
            budget = 40
    else: #creation of a oneTime customer
        budget = 100
        if self.regular_flag == True:
            customer_type = "one time regular"
        else:
            customer_type = "one time tripadvisor"

    self.history[str(self.id)] = {"customer_type" : customer_type, "budget" : budget, "commandes" : {}} #updating of the general history and creation of the personal history of the created customer
    return self.history

#PART4 : additional question : What would happen if the probability to have a returning customer is not 20% but 40% ?

def db_simulation(start_date, end_date):
    history = {}
    list_returning_id = []
    for cust in range (1000): #we create the base of database with the returning customer
        if randint(1,100) <= 33: #about a third must be hipster ones
            customer = Customers.Customer(len(history),False,False) #creation of the hipsters
            history = customer.order(history) #updating the general history with the personal history of the created customers
        else:
            customer = Customers.Customer(len(history),False,True) #creation of the returning regular customers
            history = customer.order(history) #updating the general history with the personal history of the created customers
        list_returning_id.append(str(len(history))) #stock all the id of the returning customers (in range from 0 to 999)
    #we will browse all day and all hour during 5 years

    for date in date_range(start_date, end_date):
        date = date.strftime("%Y-%m-%d")
        all_hour = coffee_df['HOUR'].unique()
        dico_simulation = {'date':[],'hour': [], 'id': [], 'drink': [], 'food': []}
        for hour in all_hour: #we browse each hour
            if randint(1,10) <=4: #that means we have a returning customer (40% of the orders)
                success = False
                while success == False:
                    rand = randint(0,999) #getting randomly the id of a returning customer
                    if history[str(rand)]['budget'] >= 10: #check if the budget is enough to buy any food and any drink
                        customer_id = str(rand)
                        customer_type = history[customer_id]["customer_type"]
                        oneTime_flag = False
                        if customer_type[-7:] == "regular":
                            regular_flag = True
                        else :
                            regular_flag = False
                        success = True
                customer_type = "returning"
            else:  #that means we have a one time customer
                if randint(1, 10) <= 1: #creation of a tripadvisor customer (10% of the one time ones)
                    customer = Customers.Customer(len(history), True, False) #creation of the customer
                    history = customer.order(history) #updating the general history with the personal history of the new customer
                else: #creation of one time regular customer
                    customer = Customers.Customer(len(history), True, True)
                    history = customer.order(history) #updating the general history with the personal history of the new customer
                customer_id = str(len(history)) #stoch the current id of the customer
                customer_type =  "one time"
            if customer_type == "returning": #if returning customer we create again the object to use it just after
                customer = Customers.Customer(customer_id, oneTime_flag, regular_flag)
            purchase = give_purchase(hour) #getting the list of the  ordered drink and food
            drink = purchase[0] #getting the ordered drink
            food = purchase[1] #getting the ordered food
            history = customer.buy_sth(drink,food,date,hour,history) #updating the general history after an order
            # updating de DF
            dico_simulation['date'].append(date)
            dico_simulation['hour'].append(hour)
            dico_simulation['id'].append(customer_id)
            dico_simulation['drink'].append(drink)
            dico_simulation['food'].append(food)
            if customer_type == "returning":    customer = Customers.Customer(customer_id, oneTime_flag, regular_flag)
            purchase = give_purchase(hour) #getting the list of the  ordered drink and food
            drink = purchase[0] #getting the ordered drink
            food = purchase[1] #getting the ordered food
            history = customer.buy_sth(drink,food,date,hour,history) #updating the general history after an order
            # updating de DF
            dico_simulation['date'].append(date)
            dico_simulation['hour'].append(hour)
            dico_simulation['id'].append(customer_id)
            dico_simulation['drink'].append(drink)
            dico_simulation['food'].append(food)


    my_df = pd.DataFrame(dico_simulation)

    return my_df

db = db_simulation(start_date, end_date)


#PART 4 (by Augustin) : Question additional:  I would like to compare the income of returning customer and the income of one-time
#in three differents periods. Indeed, we saw that a typical day can be split in 3 periods : 08:00 - 11:30 ; 11:30 - 15:00
#and 15:00-18:00. Depending on the comparison we could take a strategical decision.  For example, we could see during the
#first period the bigger part of income comes from returning customers. We could decide to focus on returning customers
#during the first period or decide to focus on how attract one-time customers.


#SOME ADDITIONAL PLOTS

#plot of average income by hour
dicoAverageIncomeHour ={'hour': [], 'averageIncomeHour':[]}
for hour in nbr_unique_hour:
    hour_df = coffee_df[coffee_df['HOUR'] == hour]
    income = 0
    i= 0
    for commande in range(len(hour_df)):
        income += give_foodPrize(hour_df.iloc[i,3 ]) + give_drinkPrize(hour_df.iloc[i,2])
        # rajout d'une colonne dans le df: income par commande
        #coffee_df['INCOME'] = give_foodPrize(coffee_df.iloc[i, 3])+ give_drinkPrize(coffee_df.iloc[i, 2])
        i += 1
    dicoAverageIncomeHour['hour'].append(hour)
    dicoAverageIncomeHour['averageIncomeHour'].append(income/len(hour_df))


averageIncomeByHour = pd.DataFrame(dicoAverageIncomeHour)
ax = averageIncomeByHour.plot(kind='bar', title='Average income by hour', figsize=(10,5), legend = True, fontsize=12)
ax.set_xlabel("Hour", fontsize=12)
ax.set_ylabel("INCOME", fontsize=12)
plt.show()

#plot of income by hour
dicoIncomeHour ={'hour': [], 'IncomeByHour':[]}
for hour in nbr_unique_hour:
    hour_df = coffee_df[coffee_df['HOUR'] == hour]
    income = 0
    i= 0
    for commande in range(len(hour_df)):
        income += give_foodPrize(hour_df.iloc[i,3 ]) + give_drinkPrize(hour_df.iloc[i,2])
        i += 1
    dicoIncomeHour['hour'].append(hour)
    dicoIncomeHour['IncomeByHour'].append(income)


incomeByHour = pd.DataFrame(dicoIncomeHour)
ax = incomeByHour.plot(kind='bar', title='Income by hour', figsize=(10,5), legend = True, fontsize=12)
ax.set_xlabel("Hour", fontsize=12)
ax.set_ylabel("INCOME", fontsize=12)
plt.show()

#plot of income by year

dicoIncomeYear={'year': nbr_year, 'IncomeByYear': []}
for year in nbr_year:
        year_df = coffee_df[coffee_df['YEAR'] == year]
        income = 0
        i = 0
        for commande in range(len(year_df)):
            income += give_foodPrize(year_df.iloc[i, 3]) + give_drinkPrize(year_df.iloc[i, 2])
            i += 1
        dicoIncomeYear['IncomeByYear'].append(income)

incomeByYear = pd.DataFrame(dicoIncomeYear)
ax = incomeByYear.plot(kind='bar', title='Income by year', figsize=(10,5), legend = True, fontsize=12)
ax.set_xlabel("Year", fontsize=12)
ax.set_ylabel("INCOME", fontsize=12)
plt.ylim([322000,324000])
plt.show()

#plot of average daily income by year

dicoAverageDailyIncome ={'year': nbr_year, 'AverageDailyIncome': []}
for year in nbr_year:
        year_df = coffee_df[coffee_df['YEAR'] == year]
        income = 0
        i = 0
        for commande in range(len(year_df)):
            income += give_foodPrize(year_df.iloc[i, 3]) + give_drinkPrize(year_df.iloc[i, 2])
            i += 1
        dicoAverageDailyIncome['AverageDailyIncome'].append(income/len(year_df))

averageDailyIncome = pd.DataFrame(dicoAverageDailyIncome)
ax = averageDailyIncome.plot(kind='bar', title='Average Daily Income by year', figsize=(10,5), legend = True, fontsize=12)
ax.set_xlabel("Year", fontsize=12)
ax.set_ylabel("INCOME", fontsize=12)
plt.ylim([5.15,5.2])
plt.show()


#plot of oneTime and returning customer:
coffee_df['RETURNING'].value_counts().plot.pie()

#plot of number of returning customer during the day

dicoReturningDay ={'hour':[], 'nbrReturCust' : []}
for hour in nbr_unique_hour:
    hour_df = coffee_df[coffee_df['HOUR'] == hour]
    nbr = (hour_df['RETURNING']== True).value_counts()
    dicoReturningDay['hour'].append(hour)
    dicoReturningDay['nbrReturCust'].append(nbr[1])

ReturingCusto = pd.DataFrame(dicoReturningDay)
ax = ReturingCusto.plot(kind='bar', title='Nbr of returning customer by hour', figsize=(10,5), legend = True, fontsize=12)
ax.set_xlabel("Hour", fontsize=12)
ax.set_ylabel("Nbr of returning custo", fontsize=12)
plt.show()

#plot of number of one-time customer during the day
dicoOneTimeDay ={'hour':[], 'nbrOneTimeCust' : []}
for hour in nbr_unique_hour:
    hour_df = coffee_df[coffee_df['HOUR'] == hour]
    nbr = (hour_df['RETURNING']== False).value_counts()
    dicoOneTimeDay['hour'].append(hour)
    dicoOneTimeDay['nbrOneTimeCust'].append(nbr[1])

oneTimeCusto = pd.DataFrame(dicoOneTimeDay)
ax = oneTimeCusto.plot(kind='bar', title='Nbr of one-time customer by hour', figsize=(10,5), legend = True, fontsize=12)
ax.set_xlabel("Hour", fontsize=12)
ax.set_ylabel("Nbr of one-time custo", fontsize=12)
plt.show()

#plot of income of one-time customer by year  #not working

dicoOneTimeIncomeYear={'year': nbr_year, 'oneTimeIncomeByYear': []}
for year in nbr_year:
        year_df = coffee_df[([coffee_df['YEAR'] == year]) & (coffee_df['RETURNING'] == False)]
        income = 0
        i = 0
        for commande in range(len(year_df)):
            income += give_foodPrize(year_df.iloc[i, 3]) + give_drinkPrize(year_df.iloc[i, 2])
            i += 1
        dicoOneTimeIncomeYear['oneTimeIncomeByYear'].append(income)

oneTimeIncomeByYear = pd.DataFrame(dicoOneTimeIncomeYear)
ax = oneTimeIncomeByYear.plot(kind='bar', title='One Time Income by year', figsize=(10,5), legend = True, fontsize=12)
ax.set_xlabel("Year", fontsize=12)
ax.set_ylabel("INCOME", fontsize=12)
plt.ylim([322000,324000])
plt.show()

#plot of income of returning customer by year #not working

dicoReturningIncomeYear={'year': nbr_year, 'returningIncomeByYear': []}
for year in nbr_year:
        year_df = coffee_df[([coffee_df['YEAR'] == year]) & (coffee_df['RETURNING'] == True)]
        income = 0
        i = 0
        for commande in range(len(year_df)):
            income += give_foodPrize(year_df.iloc[i, 3]) + give_drinkPrize(year_df.iloc[i, 2])
            i += 1
        dicoReturningIncomeYear['returningIncomeByYear'].append(income)

returningIncomeByYear = pd.DataFrame(dicoReturningIncomeYear)
ax = returningIncomeByYear.plot(kind='bar', title='Returning Income by year', figsize=(10,5), legend = True, fontsize=12)
ax.set_xlabel("Year", fontsize=12)
ax.set_ylabel("INCOME", fontsize=12)
plt.ylim([322000,324000])
plt.show()

#plot of average income of one-time customer by year #not working

dicoOneTimeAverageIncomeYear={'year': nbr_year, 'oneTimeAverageIncomeByYear': []}
for year in nbr_year:
        year_df = coffee_df[([coffee_df['YEAR'] == year]) & (coffee_df['RETURNING'] == False)]
        income = 0
        i = 0
        for commande in range(len(year_df)):
            income += give_foodPrize(year_df.iloc[i, 3]) + give_drinkPrize(year_df.iloc[i, 2])
            i += 1
        dicoOneTimeAverageIncomeYear['oneTimeAverageIncomeByYear'].append(income/len(year_df))

oneTimeAverageIncomeByYear = pd.DataFrame(dicoOneTimeAverageIncomeYear)
ax = oneTimeAverageIncomeByYear.plot(kind='bar', title='One Time Average Income by year', figsize=(10,5), legend = True, fontsize=12)
ax.set_xlabel("Year", fontsize=12)
ax.set_ylabel("INCOME", fontsize=12)
plt.show()
#plot of average income of returning customer by year  #not working

dicoReturningAverageIncomeYear={'year': nbr_year, 'returningAverageIncomeByYear': []}
for year in nbr_year:
        year_df = coffee_df[([coffee_df['YEAR'] == year]) & (coffee_df['RETURNING'] == True)]
        income = 0
        i = 0
        for commande in range(len(year_df)):
            income += give_foodPrize(year_df.iloc[i, 3]) + give_drinkPrize(year_df.iloc[i, 2])
            i += 1
        dicoReturningAverageIncomeYear['returningAverageIncomeByYear'].append(income/len(year_df))

returningAverageIncomeByYear = pd.DataFrame(dicoReturningAverageIncomeYear)
ax = returningAverageIncomeByYear.plot(kind='bar', title='Returning Average Income by year', figsize=(10,5), legend = True, fontsize=12)
ax.set_xlabel("Year", fontsize=12)
ax.set_ylabel("INCOME", fontsize=12)
plt.show()


