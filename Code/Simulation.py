from Code import Customers
import pandas as pd
from random import randint
from datetime import timedelta, date
import matplotlib.pyplot as plt

path ="./Data/"
coffee_df = pd.read_csv(path+"Coffeebar_2013-2017.csv", sep=';')
coffee_df['HOUR'] = coffee_df['TIME'].str[-8:]
coffee_df['DATE'] = coffee_df['TIME'].str[:10]
coffee_df['FOOD'] = coffee_df['FOOD'].fillna('nothing')  #to replace missing value by the str : "nothing"



#modification of the function "average customeer purchase" made in the part 1 to save the probability in a list

def average_customer_purchase(time):
    total = coffee_df[coffee_df['HOUR']== time]
    nbr_total = total.shape[0]
    all_drinks = total['DRINKS'].unique()
    all_food = total['FOOD'].unique()
    dico_food = {}
    dico_drink = {}
    for drink in all_drinks:
        nbr_precise_drink = (total[total['DRINKS'] == drink]).shape[0]
        dico_drink[drink] = round((float(nbr_precise_drink)/ nbr_total)*100, 2)

    for food in all_food:
        nbr_precise_food = (total[total['FOOD'] == food]).shape[0]
        dico_food[food] = round((float(nbr_precise_food)/nbr_total)*100, 2)
    list_purchases = [dico_drink, dico_food]
    return (list_purchases)
# function that return a drink and a food depending on the probability calculated in the function above
def give_purchase(time):

    #finding drink depending on the probability
    dico_drink = average_customer_purchase(time)[0]
    intervalle_drink_proba = {}
    temp_drink = 0
    for drink in dico_drink:
        intervalle_drink_proba[drink] = dico_drink[drink] + temp_drink
        temp_drink += dico_drink[drink]
    proba = randint(1,99)
    for purchase in intervalle_drink_proba:
        if proba <= intervalle_drink_proba[purchase]:
            drink_ordered = purchase
            break

    #finding food depending on the probability
    dico_food = average_customer_purchase(time)[1]
    intervalle_food_proba = {}
    temp_food = 0
    for food in dico_food:
        intervalle_food_proba[food] = dico_food[food] + temp_food
        temp_food += dico_food[food]
    proba = randint(1,99)
    #print('proba :', proba)
    #print('list_food : ', intervalle_food_proba)
    for purchase in intervalle_food_proba:
        if proba <= intervalle_food_proba[purchase]:
            food_ordered = purchase
            break

    return drink_ordered, food_ordered
give_purchase('08:00:00')
def give_foodPrize(food):  #function which returns the price of the ordered food
    if food in ["sandwich", "cookie", "pie", "muffin", "nothing"]:  # list of all the food we can find in the coffee bar
        if food == "sandwich":
            price = 5
        elif food == "cookie":
            price = 2
        elif food == "nothing":
            price = 0
        else:
            price = 3
    else: #error message if the ordered food is unknown or just not available in the coffee bar
        return ("The mentionned food is not available in the coffee bar")  # to avoid mistake in the program
    return price

def give_drinkPrize(drink): #function wich returns the price of the ordered drink
    if drink in ["coffee", "water", "milkshake", "tea", "frappucino", "soda"]:  #list of all the drinks we can find in the coffee bar
        if drink == "milkshake":
            price = 5
        elif drink == "frappucino":
            price = 4
        elif drink == "water":
            price = 2
        else:
            price = 3
    else: #error message if the ordered food is unknown or just not available in the coffee bar
        return ("The mentionned drink is not available in the coffee bar")
    return price

def date_range(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)


start_date = date(2018, 1, 1)
end_date = date(2023, 1, 1)


def db_simulation(start_date, end_date):
    history = {}
    list_returning_id = []
    for cust in range (1000): #we create the base of database with the returning customer
        if randint(1,100) <= 33: #about a third must be hipster ones
            customer = Customers.Customer(len(history),False,False) #creation of the hipsters
            history = customer.order(history) #updating the general history with the personal history of the created customers
        else:
            customer = Customers.Customer(len(history),False,True) #creation of the returning regular customers
            history = customer.order(history) #updating the general history with the personal history of the created customers
        list_returning_id.append(str(len(history))) #stock all the id of the returning customers (in range from 0 to 999)
    #we will browse all day and all hour during 5 years

    #delta = datetime.timedelta(days=1)
    #while start_date <= end_date: #we browse each day
        #recording of all the 171 differents hours
    dico_simulation = {'date': [], 'hour': [], 'id': [], 'drink': [], 'food': []}
    for date in date_range(start_date, end_date):
        date = date.strftime("%Y-%m-%d")
        all_hour = coffee_df['HOUR'].unique()
        for hour in all_hour: #we browse each hour
            if randint(1,10) <=2: #that means we have a returning customer (20% of the orders)
                success = False
                while success == False:
                    rand = randint(0,999) #getting randomly the id of a returning customer
                    if history[str(rand)]['budget'] >= 10: #check if the budget is enough to buy any food and any drink
                        customer_id = str(rand)
                        customer_type = history[customer_id]["customer_type"]
                        oneTime_flag = False
                        if customer_type[-7:] == "regular":
                            regular_flag = True
                        else :
                            regular_flag = False
                        success = True
                customer_type = "returning"
            else:  #that means we have a one time customer
                if randint(1, 10) <= 1: #creation of a tripadvisor customer (10% of the one time ones)
                    customer = Customers.Customer(len(history), True, False) #creation of the customer
                    history = customer.order(history) #updating the general history with the personal history of the new customer
                else: #creation of one time regular customer
                    customer = Customers.Customer(len(history), True, True)
                    history = customer.order(history) #updating the general history with the personal history of the new customer
                customer_id = str(len(history)) #stoch the current id of the customer
                customer_type =  "one time"
            if customer_type == "returning": #if returning customer we create again the object to use it just after
                Customers.Customer(customer_id, oneTime_flag, regular_flag)
            purchase = give_purchase(hour) #getting the list of the  ordered drink and food
            drink = purchase[0] #getting the ordered drink
            food = purchase[1] #getting the ordered food
            history = customer.buy_sth(drink,food,date,hour,history) #updating the general history after an order
            # updating de DF
            dico_simulation['date'].append(date)
            dico_simulation['hour'].append(hour)
            dico_simulation['id'].append(customer_id)
            dico_simulation['drink'].append(drink)
            dico_simulation['food'].append(food)
            #print(date, hour, customer_id,drink, food)

    my_df = pd.DataFrame(dico_simulation)

    return [my_df, dico_simulation]

db = db_simulation(start_date, end_date)
#to save our db in a dataframe
my_true_df = db[0]
#to save our dataframe in a file
my_true_df.to_csv('Data/Coffeebar_2018-2022.csv', sep = ';', index = False)

#SIMULATION

coffee_df_simu = pd.read_csv(path+"Coffeebar_2018-2022.csv", sep=';')
coffee_df_simu['year'] = coffee_df_simu['date'].str[:4]   # we add a row 'YEAR' to simplify our futur code
nbr_year = coffee_df_simu['year'].unique()

#plot of amount of food sold over the five year
food_by_year = coffee_df_simu['food'].value_counts()
ax = food_by_year.plot(kind='bar', title="Food by type", figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Type", fontsize=12)
ax.set_ylabel("FOOD", fontsize=12)
plt.show()


#plot of amount of drink sold over the five year
drinkByYear = coffee_df_simu['drink'].value_counts()
ax = drinkByYear.plot(kind='bar', title="Drinks by type", figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Type", fontsize=12)
ax.set_ylabel("DRINKS", fontsize=12)
plt.show()

