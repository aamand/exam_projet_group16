#plot of average income of one-time and returningg customer :

dicoAverageOneTimeIncomeYear={ 'averageIncomeOneTimeByYear': []}
returning_df = coffee_df[coffee_df['RETURNING'] == False]
income = 0
i = 0
for commande in range(len(returning_df)):
    income += give_foodPrize(returning_df.iloc[i, 3]) + give_drinkPrize(returning_df.iloc[i, 2])
    i += 1
dicoAverageOneTimeIncomeYear['averageIncomeOneTimeByYear'].append(income / len(returning_df))

#averageOneTimeIncomeYear = pd.DataFrame(dicoAverageOneTimeIncomeYear)
#ax = oneTimeCusto.plot(kind='bar', title='Nbr of one-time customer by hour', figsize=(10,5), legend = True, fontsize=12)
#ax.set_xlabel("Hour", fontsize=12)
#ax.set_ylabel("Average income", fontsize=12)
#plt.show()


dicoReturningIncomeYear={'year': nbr_year, 'IncomeReturningByYear': []}
for year in nbr_year:
        year_df = coffee_df[(coffee_df['YEAR'] == year) & (coffee_df['RETURNING'] == True)]
        income = 0
        i = 0
        for commande in range(len(year_df)):
            income += give_foodPrize(year_df.iloc[i, 3]) + give_drinkPrize(year_df.iloc[i, 2])
            # rajout d'une colonne dans le df: income par commande
            # coffee_df['INCOME'] = give_foodPrize(coffee_df.iloc[i, 3])+ give_drinkPrize(coffee_df.iloc[i, 2])
            i += 1
        dicoReturningIncomeYear['IncomeReturningByYear'].append(income)

OneTimeincomeByYear = pd.DataFrame(dicoOneTimeIncomeYear)
ReturningIncomeByYear = pd.DataFrame(dicoReturningIncomeYear)


means_men = (20, 35, 30, 35, 27)
means_women = (25, 32, 34, 20, 25)

fig, ax = plt.subplots()

index = 5
bar_width = 0.35
opacity = 0.4
error_config = {'ecolor': '0.3'}
rects1 = plt.bar(index, dicoReturningIncomeYear['IncomeReturningByYear'], bar_width,
                 alpha=opacity,
                 color='b',
                 #yerr=std_men,
                 error_kw=error_config,
                 label='Returning')

rects2 = plt.bar(index + bar_width, dicoOneTimeIncomeYear['IncomeOneTimeByYear'], bar_width,
                 alpha=opacity,
                 color='r',
                 #yerr=std_women,
                 error_kw=error_config,
                 label='OneTime')

plt.xlabel('Year')
plt.ylabel('Income')
plt.title('Income of one-time customer by year')
plt.xticks(index + bar_width / 2, nbr_year)
plt.legend()

plt.tight_layout()
plt.show()





ax = OneTimeincomeByYear.plot(kind='bar', title='Income by year', figsize=(10,5), legend = True, fontsize=12)
plt.bar( OneTimeincomeByYear, color = 'b', width = 0.25 )
plt.bar( ReturningIncomeByYear(), color = 'r', width = 0.25 )
#ax.set_xlabel("Year", fontsize=12)
ax.set_ylabel("INCOME", fontsize=12)
plt.ylim([261000,264500])
plt.show()



dico = {"1" : 2, "2" : 3, "3" : 4}
list_keys = []
for key in dico:
    list_keys.append(key)

print(list_keys)