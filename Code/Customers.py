import pandas as pd
from random import randint
#example of history
#history = {"id1" : {"type": one time regular, "budget" : XXX , "commandes" : {"date1" :["drink1","food1"], "date2" : ["drink2","food2"], ...}}, "BF1 : {...}...}

class Customer(object): #creation of a class object
    def __init__(self, id, oneTime_flag, regular_flag):
        self.id = id #id number of the customer
        self.oneTime_flag = oneTime_flag #True if the customer is a OneTime one, False if not
        self.regular_flag = regular_flag #True if the customer is a regular one, False if not

    def order (self, history): #function to update the general history with a new customer
        self.history = history #general history of the coffee bar with all the customers
        if self.oneTime_flag == False : #creation of a returning customer
            #print('Welcome back customer'+' '+str(self.id))
            if self.regular_flag == True:
                customer_type = "returning regular"
                budget = 250
            else:
                customer_type = "returning hipster"
                budget = 500
        else: #creation of a oneTime customer
            budget = 100
            if self.regular_flag == True:
                customer_type = "one time regular"
            else:
                customer_type = "one time tripadvisor"

        self.history[str(self.id)] = {"customer_type" : customer_type, "budget" : budget, "commandes" : {}} #updating of the general history and creation of the personal history of the created customer
        return self.history

    def shopping_list(self,history): #function which returns the last purchase of a customer
        self.history = history #general history of the coffee bar with all the customers
        personal_data = self.history[str(self.id)] #getting the personal history of the customer
        all_orders = personal_data["commandes"] #getting all the orders of the customer
        date_orders = []
        for key in all_orders:
            date_orders.append(key) #getting all the dates when the customer came to the coffee bar
        date_last_purchase = date_orders[0]
        for order in date_orders: #comparison of the dates to find the most recent one
            if order > date_last_purchase:
                date_last_purchase = order
        last_purchase = [personal_data["commandes"][date_last_purchase][0], personal_data["commandes"][date_last_purchase][1]] #creation of a list with the drink and the food bought in the last purchase
        return last_purchase

    def amount_spending(self, history):  #function which returns the amount of money of the last purchase (i.d. the contents of the list "last_purchase")
        self.history = history #general history of the coffee bar with all the customers
        last_purchase = Customer.shopping_list(self, history) #getting the last purchase
        amount_last_purchase = give_drinkPrize(last_purchase[0]) + give_foodPrize(last_purchase[1]) #calculation of the amount of the last purchase
        return amount_last_purchase

    def buy_sth(self, drink, food, day, hour, history): #function which returns the updated general history after a customer bought something
        self.drink = drink #ordered drink
        self.food = food #ordered food
        self.day = day #day of the order (format : "2015-12-31" = 31st December 2015)
        self.hour = hour #hour of the order (format : 08:00:00 = 8 o'clock AM)
        self.history = history #general history of the coffee bar with all the customers
        personal_data = self.history[str(self.id)] #getting the personal history of the customer
        budget = personal_data["budget"] #getting the current budget of the customer
        if personal_data["customer_type"] == "one time tripadvisor": #checking of the customer_type for the possible tip
            tip = randint(1,10) #calculation of the tip, randomly between 1 and 10
        else:
            tip = 0 #if not tripdavisor customer, no tip
        priceDrink = give_drinkPrize (drink) #getting the price of the ordered drink
        priceFood = give_foodPrize (food) #getting the price of the ordered food
        total_price = priceDrink + priceFood #calculation of the total price for the drink and the food
        total_money_spent = total_price + tip #calculation of the total amount money spent included the tip
        #print ("The price of your order is"+" "+str(total_price)+"€")
        budget = budget - total_money_spent #updating of the budget of the customer
        if budget < 0: #error message if the budget is not
            return ("Your budget is not enough for this purchase")
        else:
            date = str(day)+' '+str(hour) #gathering of te day and the hour to have the complete moment of the order
            personal_data["commandes"][date] = [drink,food] #adding the date as a key whose value is a list with 2 elements. Respectively the ordered drink and the ordered food
            personal_data["budget"] = budget #updating the budget of the customer in his personal history
            self.history[str(self.id)] = personal_data #updating the general history of the coffee bar with the updated personal history of the customer
        return self.history

    def access_history_drinkSpending(self, history): #function which returns the history in drink
        #format : a dictionary with a key for each drink whose value is a list containing the total amount consumed of this drink and the total money spent for this drink
        #an additional key is the total budget spent for drinks.
        #example of returned dictinnary : {'coffee': [1, 3], 'water': [2, 4], 'total drink budget': 7}
        #in 3 orders : 1 coffee for 3€, 2 waters for 4€ thus 7€ spent in drinks
        self.history = history #general history of the coffee bar with all the customers
        shopping_drink = {} #initialization of the dictionary
        for date in self.history[str(self.id)]["commandes"]:
            drink = self.history[str(self.id)]["commandes"][date][0] #for each date, getting the ordered drink and updating the dictionary
            if drink not in shopping_drink:
                shopping_drink[drink] = [1,0]
            else:
                shopping_drink[drink][0] += 1
        total_money_drink = 0 #initialization of the future value of the key "total drink budget"
        for drink in shopping_drink: #updating of the total consumed amount of each drink
            shopping_drink[drink][1] = shopping_drink[drink][0] * give_drinkPrize(drink)
            total_money_drink += shopping_drink[drink][1]
        shopping_drink["total drink budget"] = total_money_drink #assignment of the total amount money spent in drinks
        return shopping_drink

    def access_history_foodSpending(self, history):  #function which returns the history in food
        #format : a dictionary with a key for each food whose value is a list containing the total amount consumed of this food and the total money spent for this food
        #an additional key is the total budget spent for food.
        #example of returned dictinnary : {'nothing': [1, 0], 'cookie': [2, 4], 'total food budget': 4}
        #in 3 orders, the customer bought once no food, other twice he bought a cookie thus he spent 4€ in food.
        self.history = history #general history of the coffee bar with all the customers
        shopping_food = {} #initialization of the dictionary
        for date in self.history[str(self.id)]["commandes"]:
            food = self.history[str(self.id)]["commandes"][date][1] #for each date, getting the ordered food and updating the dictionary
            if food not in shopping_food:
                shopping_food[food] = [1,0]
            else:
                shopping_food[food][0] += 1
        total_money_food = 0 #initialization of the future value of the key "total food budget"
        for food in shopping_food: #updating of the total consumed amount of each food
            shopping_food[food][1] = shopping_food[food][0] * give_foodPrize(food)
            total_money_food += shopping_food[food][1]
        shopping_food["total food budget"] = total_money_food #assignement of the total amount money spent in food
        return shopping_food

    def access_history_budget(self, history): #function which returns the history of the budget date by date
        #format : a dictionary with a key for each ordered date with the updated budget after the order as value
        #an additional key called "initial budget" has the value corresponding to the right budget depending on the customer type
        #exampple of a returned dictionary : {'initial budget': 500, '2015-02-06 08:05:00': 497, '2016-03-01 11:00:00': 493, '2016-03-02 11:00:00': 489}
        #3 orders : first = 3€
        #           second = 4€
        #           third = 4€
        self.history = history #general history of the coffee bar with all the customers
        budget_history = {} #initialization of dictionary
        if self.history[str(self.id)]["customer_type"] == "returning hipster": #checking the customer type to assign the right budget
            budget = 500
        elif self.history[str(self.id)]["customer_type"] == "returning regular":
            budget = 250
        else:
            budget = 100
        budget_history["initial budget"] = budget #assignement of the initial budget depend on the customer type
        for date in self.history[str(self.id)]['commandes']: #loop with all dates of order
            budget_history[date] = ((budget - give_drinkPrize(self.history[str(self.id)]["commandes"][date][0])) - give_foodPrize(self.history[str(self.id)]["commandes"][date][1])) #assignmet of the date as key, and calculation of the updated budget with the order drink and food
            budget = budget_history[date]
        return budget_history

def give_foodPrize(food):  #function which returns the price of the ordered food
    if food in ["sandwich", "cookie", "pie", "muffin", "nothing"]:  # list of all the food we can find in the coffee bar
        if food == "sandwich":
            price = 5
        elif food == "cookie":
            price = 2
        elif food == "nothing":
            price = 0
        else:
            price = 3
    else: #error message if the ordered food is unknown or just not available in the coffee bar
        return ("The mentionned food is not available in the coffee bar")  # to avoid mistake in the program
    return price

def give_drinkPrize(drink): #function wich returns the price of the ordered drink
    if drink in ["coffee", "water", "milkshake", "tea", "frappucino", "soda"]:  #list of all the drinks we can find in the coffee bar
        if drink == "milkshake":
            price = 5
        elif drink == "frappucino":
            price = 4
        elif drink == "water":
            price = 2
        else:
            price = 3
    else: #error message if the ordered food is unknown or just not available in the coffee bar
        return ("The mentionned drink is not available in the coffee bar")
    return price

############### TEST ###############
#history = {} #OK
#my_custo = Customer(1, False, False) #OK
#history = my_custo.order(history) #OK
#print(history) #OK
#my_custo2 = Customer(3,True, True) #OK
#history = my_custo2.order(history) #OK
#print(history) #OK
#print(history['1']) #OK
#history = my_custo2.buy_sth('soda','pie','2015-02-06','08:00:00',history) #OK
#print(history)
#history = my_custo.buy_sth('coffee','nothing','2015-02-06','08:05:00',history) #OK
#history = my_custo.buy_sth('water','cookie','2016-03-01','11:00:00',history) #OK
#history = my_custo.buy_sth('water','cookie','2016-03-02','11:00:00',history) #OK
#print(history)#OK
#print(my_custo.shopping_list(history))  #OK
#print(my_custo.amount_spending(history)) #OK
#print(my_custo2.amount_spending(history)) #OK
#print(my_custo.access_history_drinkSpending(history)) #OK
#print(my_custo.access_history_foodSpending(history)) #OK
#print(my_custo.access_history_budget(history)) #OK