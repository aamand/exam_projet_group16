# Exam_project_group16

Coffee Bar

We are Alexandre Amand and Augustin Nollevaux. We are student at the university of Namur (UNamur).
This project takes place for the course of Data Analytics (EINGB301)   which is given by Mr de Geyter.

Our project models a coffee bar that sells drinks and foods. The goal is to simulate a five year span. The coffee bar has different types of customers (onetime/returning customers, regular customer, customer from TripAdvistor and hipster customer).
All customers are able to buy drink (and potentially food). Only returning customers can check the history of their purchases (drink, food, budget).
All the one time tripdavisor customers give a tip.
The model is also able to calculate probabilities of buying a certain thing in a certain time from the given data (2013-2017) and to tell the price of food or drink.

The directory "Code" is separated in 4 parts to answer the questions of the teachers.

The first part is Exploratory. It consists in some manipulations of the original dataframe to get some informations.
It's also in the part1 that we get the probability of buying this drink or this one depending on the hour of the order.

The second one is Customers. It's a script with the class Customer to create a new one and also all the functions which will be use to run the program.
We can find inside the specific functions for an object customer.
We can also find inside general functions to run the program like the function to return the price of a purchase.
We also added many print and calls of functions to test our code in the end.

The third part is Simulation. It consists in simulate 5 year of activities in the coffee bar.
For this part we consider we having a base of 1000 returning customers whose a third are hipsters.
We also consider than a customer who orders is only for 20% of time a returning one.
Thus in 80% of time we have a one time custome. There is 1 chanco on 10 that he's a tripadvisor customer and gives a tip (between 1 and 10€)
In this part we can so find a function to get the probability, and another to return a drink and a food when we give it a time (in hour!)
We also code a function to loop on the date. The simulation begins on 01st January 2018 and ends on 31st Dember 2022.

The last part is Additional.
Inside we answer the additional questions of the teacher. It consists to show that our functions from part2 work perfectly.
We show some buying histories of returning customers, a decrease of the number of returning customers, an increase of 20% of the prices and the budget of hipster customers droped to 40€.
We also added some plot to compare the original data from the coffee bar (2013-2017) and our simulation (2018-2022).
